FROM openjdk:15-oracle
ENV ENVIROMENT=prod

MAINTAINER Malte Böhm <boemalte@gmail.com>

ADD backend/target/uptime-alarm-bot.jar app.jar

CMD ["sh" , "-c", "java -jar -Dserver.port=$PORT -Dspring.data.mongodb.uri=$MONGO_DB_URI -Ddiscord.token=$DISCORD_TOKEN app.jar"]
