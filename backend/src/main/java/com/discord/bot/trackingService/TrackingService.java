package com.discord.bot.trackingService;


import com.discord.bot.discord.WebsiteStatusToMarkDown;
import com.discord.bot.model.PingAndDowntime;
import com.discord.bot.model.WebSite;
import com.discord.bot.mongoDao.WebSiteMongoDao;
import com.discord.bot.enums.PingStatus;
import com.discord.bot.utils.HttpUtils;
import com.discord.bot.utils.TimeUtils;
import discord4j.common.util.Snowflake;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.rest.util.Color;
import lombok.AccessLevel;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.time.Instant;
import java.util.*;


@Slf4j
@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class TrackingService {


    MongoOperations mongoOperation;
    WebSiteMongoDao websiteMongoDao;

    WebsiteStatusToMarkDown webStatus = new WebsiteStatusToMarkDown();
    TimeUtils timeUtils = new TimeUtils();
    HttpUtils httpUtils = new HttpUtils();
    String currentTimeString = timeUtils.getCurrentTimeInDaysMonthYearsHoursMin();


    @Autowired
    public TrackingService(GatewayDiscordClient client, MongoOperations mongoOperation, WebSiteMongoDao websiteMongoDao) {
        this.mongoOperation = mongoOperation;
        this.websiteMongoDao = websiteMongoDao;


        /* Tracking all WebsSite all 30 seconds */
        TimerTask repeatedTask = new TimerTask() {
            @SneakyThrows
            public void run() {
                trackAllWebsitesEveryHalfMin(client);
            }
        };

        Timer timer = new Timer("Timer");
        long delay = 1000L;
        long period = 1000L * 30L;
        timer.scheduleAtFixedRate(repeatedTask, delay, period);
    }


    private void trackAllWebsitesEveryHalfMin(GatewayDiscordClient client) throws IOException {

        var webSiteListOfMongoDb = websiteMongoDao.findAll();

        for (WebSite websiteToCheck : webSiteListOfMongoDb) {
            Snowflake channelId = Snowflake.of(websiteToCheck.getChannelId());
            var websiteString = websiteToCheck.getWebsiteUrl();
            var channel = client.getChannelById(channelId);
            var channelPresent = Optional.ofNullable(channel.cast(MessageChannel.class).block());
            var msgChannel = channelPresent.orElseGet(() -> channelPresent.orElseThrow(() -> new NullPointerException("msgChannel was Null")));
            var websiteIsOnline = httpUtils.isWebsiteOnline(websiteString);
            var websiteIsOffline = !httpUtils.isWebsiteOnline(websiteString);
            var currentWebSpeed = httpUtils.getWebsitePingInMs(websiteString);
            var slowAlarmOnInDb = websiteToCheck.isSlowAlarm();
            var slowAlarmOffInDb = !websiteToCheck.isSlowAlarm();
            var offlineAlarmOnInDb = websiteToCheck.isOfflineAlarm();
            var offlineAlarmOffOnInDb = !websiteToCheck.isOfflineAlarm();
            var lastPingsOfWebsiteInDbCount = websiteToCheck.getPingAndDowntimes().size();


            if (websiteIsOffline && offlineAlarmOffOnInDb) {
                val newOfflineWebSiteStatus = createOfflineWebSiteStatus(websiteToCheck);
                updateNewWebsiteStatusInDb(newOfflineWebSiteStatus);
                sendDiscordEmbedShowingSiteIsOffline(msgChannel, websiteString);
                continue;
            }

            if (websiteIsOnline && currentWebSpeed >= PingStatus.AVG.getMs()) {
                val newSlowWebSiteStatus = createSlowWebSiteStatus(websiteToCheck, currentWebSpeed);
                updateNewWebsiteStatusInDb(newSlowWebSiteStatus);
                sendDiscordEmbedShowingSiteAlarmSlow(msgChannel, websiteString, currentWebSpeed);
                continue;
            }

            if (websiteIsOnline && lastPingsOfWebsiteInDbCount >= 3 && offlineAlarmOnInDb
                || currentWebSpeed < PingStatus.FAST.getMs() && lastPingsOfWebsiteInDbCount >= 3 && slowAlarmOnInDb) {
                val webSiteStatusIsBackToOnline = createOnlineWebSiteStatus(websiteToCheck, currentWebSpeed);

                val lastThreePingsOfWebsite = websiteToCheck.getPingAndDowntimes().subList(lastPingsOfWebsiteInDbCount - 3, lastPingsOfWebsiteInDbCount);
                val ping1 = lastThreePingsOfWebsite.get(0).getPingInMs();
                val ping2 = lastThreePingsOfWebsite.get(1).getPingInMs();
                val ping3 = lastThreePingsOfWebsite.get(2).getPingInMs();
                val avgPing = ping1 + ping2 + ping3 / 3;

                if (avgPing <= PingStatus.AVG.getMs()) {
                    val webSiteStatusIsBackToFast = createFastWebSiteStatus(webSiteStatusIsBackToOnline, currentWebSpeed);
                    if(slowAlarmOnInDb) {
                        sendDiscordEmbedShowingSiteIsOnline(msgChannel, websiteString);
                    }
                    sendDiscordEmbedShowingSiteIsFast(msgChannel, websiteString, currentWebSpeed);
                    updateNewWebsiteStatusInDb(webSiteStatusIsBackToFast);
                }
                if (ping1 != 0 && ping2 != 0 && ping3 != 0 && offlineAlarmOnInDb) {
                    updateNewWebsiteStatusInDb(webSiteStatusIsBackToOnline);
                    sendDiscordEmbedShowingSiteIsOnline(msgChannel, websiteString);
                    continue;
                }
                continue;
            }

            if (websiteIsOnline && offlineAlarmOffOnInDb && slowAlarmOffInDb){
                val newWebSiteStatusForDb = createFastWebSiteStatus(websiteToCheck, currentWebSpeed);
                updateNewWebsiteStatusInDb(newWebSiteStatusForDb);
            }
        }
    }


    private WebSite createOfflineWebSiteStatus(WebSite websiteToUpdate) {
        var websiteString = websiteToUpdate.getWebsiteUrl();
        val getCurrentPingAndDownTimeList = websiteToUpdate.getPingAndDowntimes();
        val newPingDownTimeItem = PingAndDowntime.builder()
                .dateOfPingOrDowntime(timeUtils.getCurrentTimeInDaysMonthYearsHoursMin())
                .pingInMs(0)
                .build();
        getCurrentPingAndDownTimeList.add(newPingDownTimeItem);

        return WebSite.builder()
                .websiteUrl(websiteString)
                .channelId(websiteToUpdate.getChannelId())
                .dateOfCreation(websiteToUpdate.getDateOfCreation())
                .offlineAlarm(true)
                .slowAlarm(false)
                .pingAndDowntimes(getCurrentPingAndDownTimeList)
                .build();
    }


    private WebSite createSlowWebSiteStatus(WebSite websiteToUpdate, int currentSlowPing) {
        val getCurrentPingAndDownTimeList = websiteToUpdate.getPingAndDowntimes();
        val newPingDownTimeItem = PingAndDowntime.builder()
                .dateOfPingOrDowntime(timeUtils.getCurrentTimeInDaysMonthYearsHoursMin())
                .pingInMs(currentSlowPing)
                .build();
        getCurrentPingAndDownTimeList.add(newPingDownTimeItem);

        return WebSite.builder()
                .websiteUrl(websiteToUpdate.getWebsiteUrl())
                .channelId(websiteToUpdate.getChannelId())
                .dateOfCreation(websiteToUpdate.getDateOfCreation())
                .offlineAlarm(false)
                .slowAlarm(currentSlowPing >= PingStatus.FAST.getMs())
                .pingAndDowntimes(getCurrentPingAndDownTimeList)
                .build();
    }

    private WebSite createOnlineWebSiteStatus(WebSite websiteToUpdate, int currentPing) {
        val newPingDownTimeItem = PingAndDowntime.builder()
                .dateOfPingOrDowntime(timeUtils.getCurrentTimeInDaysMonthYearsHoursMin())
                .pingInMs(currentPing)
                .build();
        val getCurrentPingAndDownTimeList = websiteToUpdate.getPingAndDowntimes();
        getCurrentPingAndDownTimeList.add(newPingDownTimeItem);

        return WebSite.builder()
                .websiteUrl(websiteToUpdate.getWebsiteUrl())
                .channelId(websiteToUpdate.getChannelId())
                .dateOfCreation(websiteToUpdate.getDateOfCreation())
                .offlineAlarm(false)
                .slowAlarm(currentPing >= PingStatus.FAST.getMs())
                .pingAndDowntimes(getCurrentPingAndDownTimeList)
                .build();
    }

    private WebSite createFastWebSiteStatus(WebSite websiteToUpdate, int fastPing) {
        val getCurrentPingAndDownTimeList = websiteToUpdate.getPingAndDowntimes();
        val newPingDownTimeItem = PingAndDowntime.builder()
                .dateOfPingOrDowntime(timeUtils.getCurrentTimeInDaysMonthYearsHoursMin())
                .pingInMs(fastPing)
                .build();
        getCurrentPingAndDownTimeList.add(newPingDownTimeItem);

        return WebSite.builder()
                .websiteUrl(websiteToUpdate.getWebsiteUrl())
                .channelId(websiteToUpdate.getChannelId())
                .dateOfCreation(websiteToUpdate.getDateOfCreation())
                .offlineAlarm(false)
                .slowAlarm(false)
                .pingAndDowntimes(getCurrentPingAndDownTimeList)
                .build();
    }



    private void updateNewWebsiteStatusInDb(WebSite websiteToGetUpdateDataFrom) {
        var idOfWebsite = websiteToGetUpdateDataFrom.getWebsiteUrl();
        var idOfChannel = websiteToGetUpdateDataFrom.getChannelId();
        var downtimesList = websiteToGetUpdateDataFrom.getPingAndDowntimes();
        var dateOfCreation = websiteToGetUpdateDataFrom.getDateOfCreation();
        var slowAlarm = websiteToGetUpdateDataFrom.isSlowAlarm();
        var offlineAlarm = websiteToGetUpdateDataFrom.isOfflineAlarm();

        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(idOfWebsite));
        val websiteToUpdate = Optional.ofNullable(mongoOperation.findOne(query, WebSite.class))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));

        websiteToUpdate.setWebsiteUrl(idOfWebsite);
        websiteToUpdate.setChannelId(idOfChannel);
        websiteToUpdate.setDateOfCreation(dateOfCreation);
        websiteToUpdate.setPingAndDowntimes(downtimesList);
        websiteToUpdate.setSlowAlarm(slowAlarm);
        websiteToUpdate.setOfflineAlarm(offlineAlarm);

        websiteMongoDao.save(websiteToUpdate);
    }


    private void sendDiscordEmbedShowingSiteIsOnline(MessageChannel currentChannel, String paramInString) {
        currentChannel.createEmbed(spec ->
                spec.setColor(Color.GREEN)
                        .setAuthor("Your Site is back online!", paramInString, "https://upload.wikimedia.org/wikipedia/en/thumb/2/2c/Christian_cross_trans.svg/1200px-Christian_cross_trans.svg.png")
                        .setThumbnail("https://media.giphy.com/media/PlVCbKHLVut59m3nng/giphy.gif")
                        .setImage("https://media.giphy.com/media/PlVCbKHLVut59m3nng/giphy.gif")
                        .setTitle(paramInString)
                        .setDescription("""
                                Your Site ${website} is finally online...
                                """
                                .replace("${website}", paramInString))
                        .setFooter("Time:", "https://github.com/")
        ).block();
    }

    private void sendDiscordEmbedShowingSiteIsOffline(MessageChannel currentChannel, String paramInString) {
        currentChannel.createEmbed(spec ->
                spec.setColor(Color.RED)
                        .setAuthor("This Site does not exists!", paramInString, "https://upload.wikimedia.org/wikipedia/en/thumb/2/2c/Christian_cross_trans.svg/1200px-Christian_cross_trans.svg.png")
                        .setThumbnail("https://upload.wikimedia.org/wikipedia/en/thumb/2/2c/Christian_cross_trans.svg/1200px-Christian_cross_trans.svg.png")
                        .setImage("https://64.media.tumblr.com/e7cfe0074ae5fd4bd2f47735e9f53206/tumblr_mkwpqrtiJN1rsdpaso1_500.gif")
                        .setTitle(paramInString)
                        .setDescription("""
                                Check if you have written ${website} the wrong way by clicking on the link...
                                """
                                .replace("${website}", paramInString))
                        .setFooter("Time:", "https://github.com/")
        ).block();
    }

    private void sendDiscordEmbedShowingSiteIsFast(MessageChannel currentChannel, String newSite, int currentPing) {
        currentChannel.createEmbed(spec ->
                spec.setColor(Color.GREEN)
                        .setAuthor("Your Site is fast again!", newSite, "https://media.giphy.com/media/l3vRhLNLIKxdypg7m/giphy.gif")
                        .setThumbnail("https://media.giphy.com/media/l3vRhLNLIKxdypg7m/giphy.gif")
                        .setTitle(newSite)
                        .setUrl(newSite)
                        .setDescription("""
                                {online}
                                {speed}
                                {ping}
                                """
                                .replace("{online}", webStatus.onlineStatus(true))
                                .replace("{speed}", webStatus.speedStatus(currentPing))
                                .replace("{ping}", "" + currentPing))
                        .addField("Added", newSite, true)
                        .addField("Time", currentTimeString, true)
                        .addField("TTFB/Ping", "" + currentPing, false)
                        .setFooter("Time:", "https://github.com/")
                        .setTimestamp(Instant.now())
        ).block();
    }

    private void sendDiscordEmbedShowingSiteAlarmSlow(MessageChannel currentChannel, String newSite, int currentPing) {
        currentChannel.createEmbed(spec ->
                spec.setColor(Color.YELLOW)
                        .setAuthor("Your Site is not fast enough!", newSite, "https://i.pinimg.com/originals/70/a5/52/70a552e8e955049c8587b2d7606cd6a6.gif")
                        .setThumbnail("https://i.pinimg.com/originals/70/a5/52/70a552e8e955049c8587b2d7606cd6a6.gif")
                        .setTitle(newSite)
                        .setUrl(newSite)
                        .setDescription("""
                                {online}
                                {speed}
                                {ping}
                                """
                                .replace("{online}", webStatus.onlineStatus(true))
                                .replace("{speed}", webStatus.speedStatus(currentPing))
                                .replace("{ping}", "" + currentPing))
                        .addField("Added", newSite, true)
                        .addField("Time", currentTimeString, true)
                        .addField("TTFB/Ping", "" + currentPing, false)
                        .setFooter("Time:", "https://github.com/")
                        .setTimestamp(Instant.now())
        ).block();
    }
}
