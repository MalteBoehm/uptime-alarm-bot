package com.discord.bot.utils;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class IsStringUtil {

    public IsStringUtil() {
    }

    public boolean IsStringUrlUtil(String url) {
        try {
            boolean isUrl = false;
            String[] urlProtocolPattern = {"http://", "https://", "http://www", "https://www"};
            val urlArray = url.toLowerCase().split("\\.");
            for (val pattern : urlProtocolPattern) {
                if (url.contains(pattern)) {
                    if (urlArray.length >= 2) {
                        isUrl = true;
                        log.debug(url + " is a URL");
                    }
                }
            }
            return isUrl;
        } catch (RuntimeException e) {
            log.warn(e + " RuntimeException in IsStringUrlUtil with the Input of: " +url);
            return false;
        }
    }
}
