package com.discord.bot.discord.commands;

import com.discord.bot.discord.DiscordBot;
import com.discord.bot.utils.IsStringUtil;
import com.discord.bot.model.SubscribedChannel;
import com.discord.bot.service.DiscordService;

import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.channel.Channel;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import java.util.List;
import java.util.Optional;


@Slf4j
@FieldDefaults( level = AccessLevel.PRIVATE, makeFinal = true)
public class Register extends Command {

    String channelId = channel.getId().asString();
    String adminId = msg.getAuthor().isPresent()? msg.getAuthor().get().getId().asString(): "";


    public Register(DiscordBot bot, DiscordService service, Message msg, Channel channel, IsStringUtil isStringUtil) {
        super(bot, service, msg, channel, isStringUtil);
        log.warn("Register Constructor");
        if (service.findChannelId(channelId).isEmpty()){
            botReplies.add("!register Your Channel is now registered in our DB. You can now start do add Websites and Services to track");
        }
    }


    public void execute() {
        boolean canExecute = super.canExecute();

        Optional<SubscribedChannel> existingChannel = service.findChannelId(channelId);

        if (existingChannel.isPresent()) {
            log.warn("Channel is Present");
            botReplies.add("Your Channel is already registered");
            canExecute = false;
        }

        if (!canExecute) return;

        SubscribedChannel newChannel = SubscribedChannel.builder()
                .channelId(channelId)
                .adminId(adminId)
                .trackedWebsites(List.of())
                .build();
        service.registerChannel(newChannel);

        botReplies.add("New channel is now added. You can now start to add Websites and Services to track");
    }
}

