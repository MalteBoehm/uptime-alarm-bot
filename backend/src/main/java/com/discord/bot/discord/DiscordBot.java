package com.discord.bot.discord;

import com.discord.bot.discord.commands.AddSite;
import com.discord.bot.discord.commands.Command;
import com.discord.bot.discord.commands.DeleteSite;
import com.discord.bot.discord.commands.Register;
import com.discord.bot.utils.IsStringUtil;
import com.discord.bot.service.DiscordService;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.Event;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.channel.MessageChannel;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DiscordBot {
    DiscordService service;
    IsStringUtil isStringUtil = new IsStringUtil();


    @Autowired
    public DiscordBot(GatewayDiscordClient client, DiscordService service) {
        this.service = service;

        client.getEventDispatcher().on(MessageCreateEvent.class)
                .map(event -> {
                    log.trace(event.toString());
                    log.debug("Incoming message: " + event.getMessage().getContent());
                    return event.getMessage();
                })
                .filter(message -> message.getAuthor().map(user -> !user.isBot()).orElse(false))
                .filter(this::isCommand)
                .subscribe(this::isMsgCommandChecker);
        client.getEventDispatcher().on(Event.class)
                .subscribe(logs -> log.trace(logs.toString()));
    }


    private void isMsgCommandChecker(Message message) {
        Command command = null;
        val channel = message.getChannel().block();
        val messageSplitArray = message.getContent().split(" ");
        val getCommandAndParameterBack = service.assignCommandAndParam(messageSplitArray);
        val commandInString = getCommandAndParameterBack[0];
        val paramInString = getCommandAndParameterBack[1];

        switch (commandInString) {
            case "!register" -> command = new Register(this, service, message, channel, isStringUtil);
            case "!help" -> createMessage(message, "Here is your Help"); //TODO
            case "!status" -> createMessage(message, "Status"); //TODO
            case "!show" -> createMessage(message, "Show"); //TODO
            case "!add" -> command = new AddSite(this, service, message, channel, isStringUtil);
            case "!delete" -> command = new DeleteSite(this, service, message, channel, isStringUtil);
            case "!stopalarm" -> createMessage(message, "Alarm Stopped for 1 Hour"); //TODO
        }

        if (isCommand(message)) {
            assert command != null;
            log.debug(command.toString());
            command.execute();
        } else {
            createMessage(message, "Your Parameter: ${parameter} might be not a  Website OR your command: ${command} is wrong. Use !help to see all commands"
                    .replace("${parameter}", "\"" + paramInString + "\"")
                    .replace("${command}", "\"" + commandInString + "\""));
            return;
        }

        for (String reply : command.getBotReplies()) {
            assert channel != null;
            channel.createMessage(reply).subscribe();
        }
    }

    private boolean isCommand(Message message) {
        return service.isMsgCommand(message.getContent());
    }



    private void createMessage(Message message, String answer) {
        final MessageChannel channel = message.getChannel().block();
        assert channel != null;
        channel.createMessage(answer).block();
    }

}
