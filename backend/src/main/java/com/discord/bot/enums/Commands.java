package com.discord.bot.enums;


public enum Commands {
    REGISTER("!register"),
    ADD("!add"),
    STATUS("!status"),
    HELP("!help"),
    SHOW("!show"),
    DELETE("!delete");

    private final String command;

    Commands(final String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }
}
