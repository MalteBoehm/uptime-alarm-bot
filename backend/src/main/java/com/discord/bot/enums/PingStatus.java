package com.discord.bot.enums;

import lombok.Getter;

@Getter
public enum PingStatus {
    FAST(500),
    AVG(700),
    SLOW(1000),
    TOO_SLOW(1500);

    public int ms;

    PingStatus(final int ms) {
        this.ms = ms;
    }
}
