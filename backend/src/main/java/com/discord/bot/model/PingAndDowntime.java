package com.discord.bot.model;

import lombok.*;
import lombok.experimental.FieldDefaults;



@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PingAndDowntime {

    String dateOfPingOrDowntime;
    int pingInMs;
}
