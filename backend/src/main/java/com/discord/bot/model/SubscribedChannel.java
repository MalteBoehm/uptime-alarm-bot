package com.discord.bot.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "channel")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SubscribedChannel {

    @Id
    String channelId;
    String adminId;
    List<WebSite> trackedWebsites;
}
