package com.discord.bot.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "webSites")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WebSite {

    @Id
    String websiteUrl;
    String channelId;
    List<PingAndDowntime> pingAndDowntimes;
    String dateOfCreation;
    boolean slowAlarm;
    boolean offlineAlarm;
}
