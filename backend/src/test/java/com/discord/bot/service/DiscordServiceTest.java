package com.discord.bot.service;

import com.discord.bot.enums.Commands;
import com.discord.bot.model.SubscribedChannel;
import com.discord.bot.model.WebSite;
import com.discord.bot.mongoDao.DiscordChannelMongoDao;
import com.discord.bot.mongoDao.WebSiteMongoDao;
import com.discord.bot.utils.HttpUtils;
import com.discord.bot.utils.IsStringUtil;
import com.discord.bot.utils.TimeUtils;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.experimental.WithBy;
import lombok.val;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
class DiscordServiceTest {
    DiscordChannelMongoDao discordChannelMongoDao = mock(DiscordChannelMongoDao.class);
    @MockBean
    MongoOperations mongoOperation = mock(MongoOperations.class);
    @MockBean
    WebSiteMongoDao websiteMongoDao = mock(WebSiteMongoDao.class);
    @MockBean
    HttpUtils httpUtils =  mock(HttpUtils.class);
    TimeUtils timeUtil = mock(TimeUtils.class);
    @MockBean
    WebClient webClient = mock(WebClient.class);
    IsStringUtil isStringUtil= new IsStringUtil();


    List<String> commandsList = Stream.of(Commands.values())
            .map(Commands::getCommand)
            .collect(Collectors.toList());

    // Mock Constructor
    private final DiscordService discordService = DiscordService.builder()
            .discordChannelMongoDao(discordChannelMongoDao)
            .websiteMongoDao(websiteMongoDao)
            .mongoOperation(mongoOperation)
            .websiteMongoDao(websiteMongoDao)
            .httpUtils(httpUtils)
            .timeUtils(timeUtil)
            .build();

    // WHEN
    String channelId = "123456";
    String adminId = "123456";
    String isCommandStringNoParam = "!register";
    String isCommandaddSiteWithParam = "!add https://www.google.com/";
    String isCommandDeleteNoParam = "!delete";
    String isCommandDeleteWithParam = "!delete 1";
    String isCommandStringIsNotCommand = "0";
    String website = "https://www.youtube.com/watch?v=ri8OskD45vQ";
    String website2 = "http://www.youtube.com/watch?v=ri8OskD45vQ";
    String website3 = "https://youtube.com/watch?v=ri8OskD45vQ";
    String website4 = "http://youtube.com/watch?v=ri8OskD45vQ";
    SubscribedChannel channelMock = SubscribedChannel.builder()
            .channelId(channelId)
            .adminId(adminId)
            .trackedWebsites(List.of())
            .build();

    Optional<SubscribedChannel> optionalEmptyChannelMock = Optional.ofNullable(SubscribedChannel.builder()
            .build());

    Optional<SubscribedChannel> optionalChannelMock = Optional.of(channelMock);
    Optional<WebSite> optionalEmptyWebsiteMock = Optional.ofNullable(WebSite.builder().build());
    Optional<WebSite> optionalWebSiteMock = Optional.ofNullable(WebSite.builder()
            .channelId(channelId)
            .websiteUrl("www.www.de")
            .dateOfCreation("")
            .pingAndDowntimes(List.of())
            .build());


    @Test
    void findChannelId_findChannelId_should_return_isPresent() {
        // WHEN
        when(discordService.findChannelId(channelId)).thenReturn(optionalChannelMock);
        val actual = discordService.findChannelId(channelId);

        // THEN
        assertThat(actual.isPresent(), is(true));
    }

    @Test
    void findChannelId_findChannelId_should_return_isEmpty() {
        // GIVEN
        val expected = optionalEmptyChannelMock.isEmpty();

        // WHEN
        when(discordService.findChannelId(channelId)).thenReturn(optionalEmptyChannelMock);
        val actual = discordService.findChannelId(channelId);

        // THEN
        assertThat(actual.isEmpty(), is(expected));
    }

    @Test
    void registerChannel_should_return_is_Channel() {
        // GIVEN
        val expectedChannel = channelMock;

        // WHEN
        when(discordService.registerChannel(channelMock)).thenReturn(expectedChannel);
        val actual = discordService.registerChannel(channelMock);

        // THEN
        assertThat(actual, is(expectedChannel));
    }

    @Test
    void isCommand_returns_true_when_command_is_defined_withoutParam() {
        // GIVEN
        val expected = true;

        // WHEN
        val actual = discordService.isMsgCommand(isCommandStringNoParam);

        // THEN
        assertThat(expected, is(actual));
    }

    @Test
    @DisplayName("Gets msg with command and param and return boolean true if its a command")
    void isCommand_addSite_returns_true_when_command_is_defined_withParam() {
        // GIVEN
        val expected = true;

        // WHEN
        val actual = discordService.isMsgCommand(isCommandaddSiteWithParam);

        // THEN
        assertThat(actual, is(expected));
    }

    @Test
    @DisplayName("Gets msg with command and NO param and return boolean false if its a command")
    void isCommand_addSite_returns_false_when_command_is_not_defined() {
        // WHEN
        val actual = discordService.isMsgCommand(isCommandStringIsNotCommand);

        // THEN
        assertThat(actual, is(false));
    }

    @Test
    @DisplayName("Gets msg with command and NO param and return boolean false if its a command")
    void isCommand_delete_WithParam_returns_true_when_command_is_defined() {
        // WHEN
        val actual = discordService.isMsgCommand(isCommandDeleteWithParam);

        // THEN
        assertThat(actual, is(true));
    }

    @Test
    @DisplayName("Gets msg with command and NO param and return boolean false if its a command")
    void isCommand_delete_WithNoParam_returns_true_when_command_is_defined() {
        // WHEN
        val actual = discordService.isMsgCommand(isCommandDeleteNoParam);

        // THEN
        assertThat(actual, is(true));
    }

    @Test
    void isWebSiteWithChannelInDB_return_optional_is_present_when_site_is_in_DB() {
        // GIVEN
        val expected = optionalWebSiteMock.isPresent();

        // WHEN
        when(discordService.isWebSiteWithChannelInDB(channelId, website)).thenReturn(optionalWebSiteMock);
        val actual = discordService.isWebSiteWithChannelInDB(channelId, website);

        // THEN
        assertThat(expected, is(actual.isPresent()));
    }

    @Test
    void isWebSiteWithChannelInDB_return_optional_is_empty_when_site_was_not_found() {
        // GIVEN
        val expected = optionalEmptyWebsiteMock.isEmpty();

        // WHEN
        when(discordService.isWebSiteWithChannelInDB(channelId, website)).thenReturn(optionalWebSiteMock);
        val actual = discordService.isWebSiteWithChannelInDB(channelId, website);

        // THEN
        assertThat(expected, is(actual.isEmpty()));
    }



    @ParameterizedTest
    @CsvSource({"!add",
            "!delete",
            "!help",
            "!register",
            "!show",
    })
    void isMsgCommand_returns_true(String discordCommand) {
        //THEN
        assertThat(commandsList.contains(discordCommand), is(true));
    }

    @ParameterizedTest
    @CsvSource({"!Add",
            "!deleted",
            "!Help",
            "!Register",
            "!Show",
    })
    void isMsgCommand_returns_false(String discordCommand) {
        //THEN
        assertThat(commandsList.contains(discordCommand), is(false));
    }

    @Test
    void findWebsiteByChannelIdAndSiteToDelete(){

        // Throws:
        //IllegalArgumentException – in case the given entities or one of its entities is null.
        // verify that
    }

    @Test
    void updateDbWithChannelWithWebsite_return_UpdatedChannel_WithNewSite() {
        // GIVEN
        Query query = new Query();
        val channelId = "1";
        query.addCriteria(Criteria.where("_id").is(channelId));
        val foundChannel = SubscribedChannel.builder()
                .channelId(channelId)
                .adminId("adminId")
                .trackedWebsites(List.of())
                .build();
        val newSite = WebSite.builder()
                .websiteUrl("abc.de")
                .channelId(channelId)
                .build();

        val expected = SubscribedChannel.builder()
                .channelId("1")
                .adminId("adminId")
                .trackedWebsites(List.of(newSite))
                .build();

        when(mongoOperation.findOne(query, SubscribedChannel.class)).thenReturn(foundChannel);
        when(mongoOperation.save(foundChannel)).thenReturn(expected);
        // WHEN
        val actual = discordService.updateDbWithChannelWithWebsite("1", newSite);

        // THEN
        assertThat(actual.getTrackedWebsites(), is(expected.getTrackedWebsites()));
    }

    @Test
    void updateDbWithChannel_return_Test(){
        // GIVEN
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is("1"));
        val oldChannel = SubscribedChannel.builder()
                .channelId("1")
                .adminId("adminId")
                .trackedWebsites(List.of())
                .build();

        val updatedChannel = SubscribedChannel.builder()
                .channelId("21")
                .trackedWebsites(List.of())
                .adminId("differentAdminId")
                .build();

        val expected = SubscribedChannel.builder()
                .channelId("21")
                .adminId("differentAdminId")
                .trackedWebsites(List.of())
                .build();

        when(mongoOperation.findOne(query, SubscribedChannel.class)).thenReturn(oldChannel);
        when(mongoOperation.save(updatedChannel)).thenReturn(expected);
        // WHEN
        val actual = discordService.updateDbWithChannel("1", updatedChannel);

        // THEN
        assertThat(actual, is(expected));
    }
    @Test
    void addNewWebsitesToTrackToDb() {
        // GIVEN
        val channelIdAsString = "007";
        val timeString = timeUtil.getCurrentTimeInDaysMonthYearsHoursMin();
        val site = "https://www.bild.de";
        val siteWithSlash = "https://www.bild.de/";
        WebSite newSite = WebSite.builder()
                .websiteUrl(site)
                .dateOfCreation(timeString)
                .channelId(channelIdAsString)
                .build();

        WebSite expected = WebSite.builder()
                .channelId(channelIdAsString)
                .websiteUrl(siteWithSlash)
                .channelId(channelIdAsString)
                .dateOfCreation(timeString)
                .pingAndDowntimes(List.of())
                .offlineAlarm(false)
                .slowAlarm(false)
                .build();
        // WHEN
        when(websiteMongoDao.save(newSite)).thenReturn(expected);
        when(discordService.addNewWebsitesToTrackToDb(channelIdAsString, newSite, timeString)).thenReturn(expected);
        val test = discordService.addNewWebsitesToTrackToDb(channelIdAsString,newSite,timeString );
        // THEN
        assertThat(test, is(expected));
    }

    @Test
    void isWebSiteWithChannelInDB() {
    }

    @Test
    void assignCommandAndParam_gets_Array_size_0_returns_array_size_1() {
        // GIVEN
        String[] emptyStringInput = {""};
        String[] expectedStringOutput = {"",""};
        val outputLength = expectedStringOutput.length;

        // WHEN
        val actual = discordService.assignCommandAndParam(emptyStringInput);

        // THEN
        assertThat(actual,is(expectedStringOutput));
        assertThat(outputLength,is(actual.length));
    }

    @Test
    void assignCommandAndParam_gets_deleteCommand_And_Number() {
        // GIVEN
        String[] inputArray = {"!delete", "1"};
        String[] expectedStringOutput = {"!delete", "1"};
        val outputLength = expectedStringOutput.length;

        // WHEN
        val actual = discordService.assignCommandAndParam(inputArray);

        // THEN
        assertThat(actual,is(expectedStringOutput));
        assertThat(outputLength,is(actual.length));
    }

    @Test
    void getAllWebsitesOfChannel(){
        // GIVEN
        val channelID = "801822238293622828";
        List<String> listOfTwoItems = new ArrayList<>(List.of("",""));
        val website = WebSite.builder().websiteUrl("").build();
        val optChannel = Optional.of(SubscribedChannel.builder()
                .channelId(channelID)
                .trackedWebsites(List.of(website,website))
                .build());

        // WHEN
        when(discordService.findChannelId(channelID)).thenReturn(optChannel);
        when(discordChannelMongoDao.findSubscribedChannelByChannelId(channelID)).thenReturn(optChannel);

        val actual = discordService.getAllWebsitesOfChannel(channelID);

        //THEN
        assertThat(actual.size(), is(listOfTwoItems.size()));
    }
}
